# RUSOutlook

## Installation

To run the app make sure you have docker installed.

```bash
git clone https://gitlab.com/alex-hse-repository/rusoutlook.git
cd rusoutlook
pip install poetry
poetry install
poetry shell
```

## Run
```bash
cd db
docker-compose up
cd ..
python3 main.py
```

## Documentation

Is available as swagger web-page
