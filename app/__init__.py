from flask import Flask
from flask_migrate import Migrate
from flask_restx import Api
from flask_sqlalchemy import SQLAlchemy

from app.config import config_by_name

db = SQLAlchemy()
api = Api()

from app.controller.meeting import api as meeting_ns
from app.controller.user import api as user_ns

api.add_namespace(user_ns, path="/user")
api.add_namespace(meeting_ns, path="/meeting")


def create_app(config_name):
    app = Flask(__name__)
    app.config.from_object(config_by_name[config_name])
    db.init_app(app)
    api.init_app(app)
    with app.app_context():
        db.create_all()
    Migrate(app, db)
    return app
