from flask_restx import Resource

from app.dao.user import UserDao
from app.dto import UserDto

api = UserDto.api
_user = UserDto.user


@api.route("/<int:id>")
@api.param("id", "The User identifier")
@api.response(404, "User not found.")
class UserAPI(Resource):
    """User API"""

    @api.doc("Create user")
    @api.response(201, "User created")
    @api.response(409, "User already exists!")
    @api.expect(_user)
    def post(self, id):
        code = UserDao.create(api.payload)
        return "", code

    @api.doc("Get user")
    @api.marshal_with(_user)
    def get(self, id):
        user, code = UserDao.get(id=id)
        return user, code

    @api.doc("Delete user")
    @api.response(200, "User deleted")
    def delete(self, id):
        code = UserDao.delete(id)
        return code

    @api.doc("Update user")
    @api.response(200, "User update")
    @api.expect(_user)
    def put(self, id):
        code = UserDao.update(id, api.payload)
        return code
