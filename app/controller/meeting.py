from flask_restx import Resource

from app.dao.meeting import MeetingDao
from app.dto import MeetingDto

api = MeetingDto.api
_meeting = MeetingDto.meeting


@api.route("/<int:id>")
@api.param("id", "The Meeting identifier")
@api.response(404, "Meeting not found.")
class MeetingAPI(Resource):
    """Meeting API"""

    @api.doc("Create meeting")
    @api.response(201, "Meeting created")
    @api.response(409, "Meeting already exists!")
    @api.expect(_meeting)
    def post(self, id):
        code = MeetingDao.create(api.payload)
        return "", code

    @api.doc("Get meeting")
    @api.marshal_with(_meeting)
    def get(self, id):
        meeting, code = MeetingDao.get(id=id)
        return meeting, code

    @api.doc("Delete meeting")
    @api.response(200, "Meeting deleted")
    def delete(self, id):
        code = MeetingDao.delete(id)
        return code

    @api.doc("Update meeting")
    @api.response(200, "Meeting update")
    @api.expect(_meeting)
    def put(self, id):
        code = MeetingDao.update(id, api.payload)
        return code
