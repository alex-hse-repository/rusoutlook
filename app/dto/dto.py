from flask_restx import Namespace, fields


class UserDto:
    api = Namespace("user", description="User related operations")
    user = api.model(
        "user",
        {
            "id": fields.Integer(required=True, description="user id"),
            "name": fields.String(required=True, description="user name"),
            "surname": fields.String(required=True, description="user surname"),
            "email": fields.String(required=True, description="user email address"),
            "status": fields.String(description="user status"),
        },
    )


class MeetingDto:
    api = Namespace("meeting", description="Meeting related operations")
    meeting = api.model(
        "meeting",
        {
            "id": fields.Integer(required=True, description="meeting id"),
            "title": fields.String(required=True, description="meeting title"),
            "start_time": fields.DateTime(required=True, description="meeting surname"),
            "end_time": fields.DateTime(required=True, description="meeting email address"),
            "status": fields.String(description="meeting status"),
            "place": fields.String(description="meeting place"),
            "description": fields.String(description="meeting description"),
            "period": fields.String(description="meeting period"),
        },
    )
