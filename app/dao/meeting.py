from app.dao.utils import delete_object, save_changes, save_object
from app.model import Meeting


class DAO:
    def get(self, id):
        meeting = Meeting.query.filter_by(id=id).first()
        if not meeting:
            return "", 404
        return meeting, 200

    def create(self, data):
        meeting = Meeting.query.filter_by(email=data["email"]).first()
        if not meeting:
            new_meeting = Meeting(
                id=data["id"],
                title=data["title"],
                start_time=data["start_time"],
                end_time=data["end_time"],
                status=data["status"],
                place=data["place"],
                description=data["description"],
                period=data["period"],
            )
            save_object(new_meeting)
            return 201
        return 409

    def update(self, id, data):
        meeting = Meeting.query.filter_by(id=id).first()
        meeting.title = data["title"]
        meeting.start_time = data["start_time"]
        meeting.end_time = data["end_time"]
        meeting.status = data["status"]
        meeting.place = data["place"]
        meeting.description = data["description"]
        meeting.period = data["period"]
        save_changes()

    def delete(self, id):
        meeting = Meeting.query.filter_by(id=id).first()
        if not meeting:
            return 404
        delete_object(meeting)
        return 200


MeetingDao = DAO()
