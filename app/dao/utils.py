from app import db


def save_object(obj):
    db.session.add(obj)
    db.session.commit()


def delete_object(obj):
    db.session.delete(obj)
    db.session.commit()


def save_changes():
    db.session.commit()
