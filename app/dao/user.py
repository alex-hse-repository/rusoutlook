from app.dao.utils import delete_object, save_changes, save_object
from app.model import User


class DAO:
    def get(self, id):
        user = User.query.filter_by(id=id).first()
        if not user:
            return "", 404
        return user, 200

    def create(self, data):
        user = User.query.filter_by(email=data["email"]).first()
        if not user:
            new_user = User(
                id=data["id"], name=data["name"], surname=data["surname"], email=data["email"], status=data["status"]
            )
            save_object(new_user)
            return 201
        return 409

    def update(self, id, data):
        user = User.query.filter_by(id=id).first()
        user.name = data["name"]
        user.surname = data["surname"]
        user.email = data["email"]
        user.status = data["status"]
        save_changes()

    def delete(self, id):
        user = User.query.filter_by(id=id).first()
        if not user:
            return 404
        delete_object(user)
        return 200


UserDao = DAO()
