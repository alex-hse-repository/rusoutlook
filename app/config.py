import os

basedir = os.path.abspath(os.path.dirname(__file__))


class Config:
    DEBUG = False
    RESTX_MASK_SWAGGER = False


class DevelopmentConfig(Config):
    DEBUG = True
    SQLALCHEMY_DATABASE_URI = "sqlite:///" + os.path.join(basedir, "rusoutlook.db")
    SQLALCHEMY_ECHO = True
    SQLALCHEMY_TRACK_MODIFICATIONS = False


class ProductionConfig(Config):
    DEBUG = False
    SQLALCHEMY_DATABASE_URI = "postgresql+psycopg2://test:password@localhost:5432/my_database"


config_by_name = dict(dev=DevelopmentConfig, prod=ProductionConfig)
